﻿namespace CatalogWinFormsApp
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            databaseConfigLabel = new Label();
            configureDatabaseButton = new Button();
            createDatabaseCheckBox = new CheckBox();
            createSchemaCheckBox = new CheckBox();
            insertDataCheckBox = new CheckBox();
            serverNameLabel = new Label();
            serverNameTextBox = new TextBox();
            configurationDBLabel = new Label();
            optionsDescriptionLabel1 = new Label();
            optionsDescriptionLabel2 = new Label();
            optionsDescriptionLabel3 = new Label();
            optionsDescriptionLabel4 = new Label();
            boldOptionsDescriptionLabel1 = new Label();
            boldOptionsDescriptionLabel2 = new Label();
            boldOptionsDescriptionLabel4 = new Label();
            boldOptionsDescriptionLabel3 = new Label();
            SuspendLayout();
            // 
            // databaseConfigLabel
            // 
            databaseConfigLabel.AutoSize = true;
            databaseConfigLabel.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            databaseConfigLabel.Location = new Point(80, 9);
            databaseConfigLabel.Name = "databaseConfigLabel";
            databaseConfigLabel.Size = new Size(286, 32);
            databaseConfigLabel.TabIndex = 0;
            databaseConfigLabel.Text = "Database Configuration";
            // 
            // configureDatabaseButton
            // 
            configureDatabaseButton.Location = new Point(159, 394);
            configureDatabaseButton.Name = "configureDatabaseButton";
            configureDatabaseButton.Size = new Size(137, 23);
            configureDatabaseButton.TabIndex = 1;
            configureDatabaseButton.Text = "Configure";
            configureDatabaseButton.UseVisualStyleBackColor = true;
            configureDatabaseButton.Click += ConfigureDatabaseButton_Click;
            // 
            // createDatabaseCheckBox
            // 
            createDatabaseCheckBox.AutoSize = true;
            createDatabaseCheckBox.Location = new Point(175, 281);
            createDatabaseCheckBox.Name = "createDatabaseCheckBox";
            createDatabaseCheckBox.Size = new Size(111, 19);
            createDatabaseCheckBox.TabIndex = 2;
            createDatabaseCheckBox.Text = "Create Database";
            createDatabaseCheckBox.UseVisualStyleBackColor = true;
            createDatabaseCheckBox.CheckedChanged += CreateDatabaseCheckBox_CheckedChanged;
            // 
            // createSchemaCheckBox
            // 
            createSchemaCheckBox.AutoSize = true;
            createSchemaCheckBox.Location = new Point(175, 310);
            createSchemaCheckBox.Name = "createSchemaCheckBox";
            createSchemaCheckBox.Size = new Size(105, 19);
            createSchemaCheckBox.TabIndex = 3;
            createSchemaCheckBox.Text = "Create Schema";
            createSchemaCheckBox.UseVisualStyleBackColor = true;
            createSchemaCheckBox.CheckedChanged += CreateSchemaCheckBox_CheckedChanged;
            // 
            // insertDataCheckBox
            // 
            insertDataCheckBox.AutoSize = true;
            insertDataCheckBox.Location = new Point(175, 340);
            insertDataCheckBox.Name = "insertDataCheckBox";
            insertDataCheckBox.Size = new Size(131, 19);
            insertDataCheckBox.TabIndex = 4;
            insertDataCheckBox.Text = "Insert Data to Tables";
            insertDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // serverNameLabel
            // 
            serverNameLabel.AutoSize = true;
            serverNameLabel.Location = new Point(44, 237);
            serverNameLabel.Name = "serverNameLabel";
            serverNameLabel.Size = new Size(80, 15);
            serverNameLabel.TabIndex = 5;
            serverNameLabel.Text = "Server Name: ";
            // 
            // serverNameTextBox
            // 
            serverNameTextBox.Location = new Point(134, 233);
            serverNameTextBox.Name = "serverNameTextBox";
            serverNameTextBox.PlaceholderText = "(localdb)\\MSSQLLocalDB";
            serverNameTextBox.Size = new Size(227, 23);
            serverNameTextBox.TabIndex = 6;
            // 
            // configurationDBLabel
            // 
            configurationDBLabel.AutoSize = true;
            configurationDBLabel.Location = new Point(28, 60);
            configurationDBLabel.MaximumSize = new Size(420, 400);
            configurationDBLabel.Name = "configurationDBLabel";
            configurationDBLabel.Size = new Size(414, 30);
            configurationDBLabel.TabIndex = 7;
            configurationDBLabel.Text = "Welcome to the Database Configuration Wizard. Please make your selections below to tailor the setup according to your needs:";
            // 
            // optionsDescriptionLabel1
            // 
            optionsDescriptionLabel1.AutoSize = true;
            optionsDescriptionLabel1.Location = new Point(149, 111);
            optionsDescriptionLabel1.MaximumSize = new Size(300, 400);
            optionsDescriptionLabel1.Name = "optionsDescriptionLabel1";
            optionsDescriptionLabel1.Size = new Size(186, 15);
            optionsDescriptionLabel1.TabIndex = 8;
            optionsDescriptionLabel1.Text = "Default server name is predefined.";
            // 
            // optionsDescriptionLabel2
            // 
            optionsDescriptionLabel2.AutoSize = true;
            optionsDescriptionLabel2.Location = new Point(149, 136);
            optionsDescriptionLabel2.MaximumSize = new Size(300, 400);
            optionsDescriptionLabel2.Name = "optionsDescriptionLabel2";
            optionsDescriptionLabel2.Size = new Size(285, 15);
            optionsDescriptionLabel2.TabIndex = 9;
            optionsDescriptionLabel2.Text = "Create a new database. Useful if you're starting fresh.";
            // 
            // optionsDescriptionLabel3
            // 
            optionsDescriptionLabel3.AutoSize = true;
            optionsDescriptionLabel3.Location = new Point(149, 166);
            optionsDescriptionLabel3.MaximumSize = new Size(300, 400);
            optionsDescriptionLabel3.Name = "optionsDescriptionLabel3";
            optionsDescriptionLabel3.Size = new Size(283, 15);
            optionsDescriptionLabel3.TabIndex = 10;
            optionsDescriptionLabel3.Text = "Establish the new data structure within the database.";
            // 
            // optionsDescriptionLabel4
            // 
            optionsDescriptionLabel4.AutoSize = true;
            optionsDescriptionLabel4.Location = new Point(149, 196);
            optionsDescriptionLabel4.MaximumSize = new Size(300, 400);
            optionsDescriptionLabel4.Name = "optionsDescriptionLabel4";
            optionsDescriptionLabel4.Size = new Size(195, 15);
            optionsDescriptionLabel4.TabIndex = 11;
            optionsDescriptionLabel4.Text = "Populate the tables with initial data.";
            // 
            // boldOptionsDescriptionLabel1
            // 
            boldOptionsDescriptionLabel1.AutoSize = true;
            boldOptionsDescriptionLabel1.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            boldOptionsDescriptionLabel1.Location = new Point(31, 110);
            boldOptionsDescriptionLabel1.MaximumSize = new Size(100, 400);
            boldOptionsDescriptionLabel1.Name = "boldOptionsDescriptionLabel1";
            boldOptionsDescriptionLabel1.Size = new Size(84, 15);
            boldOptionsDescriptionLabel1.TabIndex = 12;
            boldOptionsDescriptionLabel1.Text = "Server Name:";
            // 
            // boldOptionsDescriptionLabel2
            // 
            boldOptionsDescriptionLabel2.AutoSize = true;
            boldOptionsDescriptionLabel2.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            boldOptionsDescriptionLabel2.Location = new Point(31, 137);
            boldOptionsDescriptionLabel2.MaximumSize = new Size(110, 400);
            boldOptionsDescriptionLabel2.Name = "boldOptionsDescriptionLabel2";
            boldOptionsDescriptionLabel2.Size = new Size(101, 15);
            boldOptionsDescriptionLabel2.TabIndex = 13;
            boldOptionsDescriptionLabel2.Text = "Create Database:";
            // 
            // boldOptionsDescriptionLabel4
            // 
            boldOptionsDescriptionLabel4.AutoSize = true;
            boldOptionsDescriptionLabel4.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            boldOptionsDescriptionLabel4.Location = new Point(31, 196);
            boldOptionsDescriptionLabel4.MaximumSize = new Size(100, 400);
            boldOptionsDescriptionLabel4.Name = "boldOptionsDescriptionLabel4";
            boldOptionsDescriptionLabel4.Size = new Size(69, 15);
            boldOptionsDescriptionLabel4.TabIndex = 14;
            boldOptionsDescriptionLabel4.Text = "Insert Data";
            // 
            // boldOptionsDescriptionLabel3
            // 
            boldOptionsDescriptionLabel3.AutoSize = true;
            boldOptionsDescriptionLabel3.Font = new Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            boldOptionsDescriptionLabel3.Location = new Point(31, 166);
            boldOptionsDescriptionLabel3.MaximumSize = new Size(110, 400);
            boldOptionsDescriptionLabel3.Name = "boldOptionsDescriptionLabel3";
            boldOptionsDescriptionLabel3.Size = new Size(91, 15);
            boldOptionsDescriptionLabel3.TabIndex = 15;
            boldOptionsDescriptionLabel3.Text = "Create Schema";
            // 
            // ConfigForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(468, 440);
            Controls.Add(boldOptionsDescriptionLabel3);
            Controls.Add(boldOptionsDescriptionLabel4);
            Controls.Add(boldOptionsDescriptionLabel2);
            Controls.Add(boldOptionsDescriptionLabel1);
            Controls.Add(optionsDescriptionLabel4);
            Controls.Add(optionsDescriptionLabel3);
            Controls.Add(optionsDescriptionLabel2);
            Controls.Add(optionsDescriptionLabel1);
            Controls.Add(configurationDBLabel);
            Controls.Add(serverNameTextBox);
            Controls.Add(serverNameLabel);
            Controls.Add(insertDataCheckBox);
            Controls.Add(createSchemaCheckBox);
            Controls.Add(createDatabaseCheckBox);
            Controls.Add(configureDatabaseButton);
            Controls.Add(databaseConfigLabel);
            Name = "ConfigForm";
            Text = "ConfigForm";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label databaseConfigLabel;
        private Button configureDatabaseButton;
        private CheckBox createDatabaseCheckBox;
        private CheckBox createSchemaCheckBox;
        private CheckBox insertDataCheckBox;
        private Label serverNameLabel;
        private TextBox serverNameTextBox;
        private Label configurationDBLabel;
        private Label optionsDescriptionLabel1;
        private Label optionsDescriptionLabel2;
        private Label optionsDescriptionLabel3;
        private Label optionsDescriptionLabel4;
        private Label boldOptionsDescriptionLabel1;
        private Label boldOptionsDescriptionLabel2;
        private Label boldOptionsDescriptionLabel4;
        private Label boldOptionsDescriptionLabel3;
    }
}