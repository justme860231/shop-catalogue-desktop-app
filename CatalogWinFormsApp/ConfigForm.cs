﻿using Microsoft.Extensions.Configuration;
using CatalogWinFormsApp.DB;
using CatalogWinFormsApp.Utils;

namespace CatalogWinFormsApp
{
    public partial class ConfigForm : Form
    {
        Action _testConnection;
        IConfiguration config;
        public ConfigForm(Action TestConnection)
        {
            InitializeComponent();
            InitializeConfigFile();
            _testConnection = TestConnection;
        }

        private void InitializeConfigFile()
        {
            config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        private void ConfigureDatabaseButton_Click(object sender, EventArgs e)
        {
            string message = "";

            if (serverNameTextBox.Text.Length > 0)
            {
                ServerChanger serverChanger = new ServerChanger("appsettings.json");
                serverChanger.SetServerName(serverNameTextBox.Text);
                message += "Server Name changed.";
            }

            if (serverNameTextBox.Text.Length != 0 || createDatabaseCheckBox.Checked || createSchemaCheckBox.Checked || insertDataCheckBox.Checked)
            {
                InitializeConfigFile();
            }

            DBInitializer databaseInit = new DBInitializer(config);

            bool isError = false;

            if (createDatabaseCheckBox.Checked && !isError)
            {
                if (databaseInit.CreateDatabase())
                {
                    message += "Database successfuly created. ";
                }
                else
                {
                    isError = true;
                }
            }
            if (createSchemaCheckBox.Checked && !isError)
            {
                if (databaseInit.CreateSchema())
                {
                    message += "Schema successfuly created. ";
                }
                else
                {
                    isError = true;
                }
            }
            if (insertDataCheckBox.Checked && !isError)
            {
                if (databaseInit.InsertInToTables())
                {
                    message += "All data inserted in to the tables.";
                }
                else
                {
                    isError = true;
                }
            }

            if (message.Length == 0)
            {
                message = "No changes were made.";
            }

            if (MessageBox.Show(message, "Confirmation", MessageBoxButtons.OK) == DialogResult.OK)
            {
                if (message != "No changes were made.")
                {
                    _testConnection();
                }

                this.Close();
            }

        }

        private void CreateDatabaseCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            createSchemaCheckBox.Checked = true;
            insertDataCheckBox.Checked = true;
        }

        private void CreateSchemaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            insertDataCheckBox.Checked = true;
        }
    }
}
