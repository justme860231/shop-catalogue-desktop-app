using System.Data;
using System.Globalization;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using CatalogWinFormsApp.Data;
using CatalogWinFormsApp.Models;

namespace CatalogWinFormsApp.DB
{
    public class DBInitializer
    {
        private readonly IConfiguration _config;
        public DBInitializer(IConfiguration config)
        {
            _config = config;
        }

        public bool CreateDatabase()
        {
            bool operationSuccessful = true;

            IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("StartingConnection"));
           
            string databaseCreateSql = "";
            
            try
            {
                databaseCreateSql = System.IO.File.ReadAllText("SQLScripts/Database.sql");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Database.sql file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            try
            {
                dbConnection.Query(databaseCreateSql);
                Console.WriteLine("Database successfully created !");
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Error at creating DB: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            return operationSuccessful;
        }

        public bool CreateSchema()
        {
            bool operationSuccessful = true;

            IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));

            string databaseCreateSql = "";

            try
            {
                databaseCreateSql = System.IO.File.ReadAllText("SQLScripts/Schema.sql");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Schema.sql file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            try
            {
                dbConnection.Query(databaseCreateSql);
                Console.WriteLine("Schema successfully created !");
            }
            catch (Exception ex) 
            {
                MessageBox.Show("Error at creating Schema: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            return operationSuccessful;
        }

        public bool InsertInToTables()
        {
            bool operationSuccessful = true;

            DataContextDapper dataContextDapper = new DataContextDapper(_config);

            string tableCreateSql = "";

            try
            {
                tableCreateSql = System.IO.File.ReadAllText("SQLScripts/Products.sql");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Products.sql file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            try
            {
                dataContextDapper.ExecuteSql(tableCreateSql);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at creating Tables: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            // --------------------------------------------------------------
            // Product Table INSERT
            // --------------------------------------------------------------
            string categoryJson = "";

            try
            {
                categoryJson = System.IO.File.ReadAllText("JSON/Category.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Category.json file: " + ex.Message, "Error");
            }

            IEnumerable<Category>? categories = JsonConvert.DeserializeObject<IEnumerable<Category>>(categoryJson);

            if (categories != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Category ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Category (CategoryId"
                                    + ",CategoryName)"
                                    + "VALUES";
                    foreach (Category singleCategory in categories)
                    {
                        string sqlToAdd = "(" + singleCategory.CategoryId
                                    + ", '" + singleCategory.CategoryName
                                    + "'),";

                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Category ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Category (CategoryId"
                                    + ",CategoryName)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
            }
            dataContextDapper.ExecuteSql("SET IDENTITY_INSERT ShopCatalogueSchema.Category OFF");

            // --------------------------------------------------------------
            // Supplier Table INSERT
            // --------------------------------------------------------------
            string supplierJson = "";

            try
            {
                supplierJson = System.IO.File.ReadAllText("JSON/Supplier.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Supplier.json file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            IEnumerable<Supplier>? suppliers = JsonConvert.DeserializeObject<IEnumerable<Supplier>>(supplierJson);

            if (suppliers != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Supplier ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Supplier (SupplierId"
                                    + ",SupplierName"
                                    + ",SupplierContact)"
                                    + "VALUES";
                    foreach (Supplier singleSupplier in suppliers)
                    {
                        string sqlToAdd = "(" + singleSupplier.SupplierId
                                    + ", '" + singleSupplier.SupplierName
                                    + "', '" + singleSupplier.SupplierContact
                                    + "'),";
                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Supplier ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Supplier (SupplierId"
                                    + ",SupplierName"
                                    + ",SupplierContact)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
                dataContextDapper.ExecuteSql("SET IDENTITY_INSERT ShopCatalogueSchema.Supplier OFF");
            }

            // --------------------------------------------------------------
            // Parameter Table INSERT
            // --------------------------------------------------------------
            string parameterJson = "";

            try
            {
                parameterJson = System.IO.File.ReadAllText("JSON/Parameter.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Parameter.json file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            IEnumerable<Parameter>? parameters = JsonConvert.DeserializeObject<IEnumerable<Parameter>>(parameterJson);

            if (parameters != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Parameter ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Parameter (ParameterId"
                                    + ",ParameterName)"
                                    + "VALUES";
                    foreach (Parameter singleParameter in parameters)
                    {
                        string sqlToAdd = "(" + singleParameter.ParameterId
                                    + ", '" + singleParameter.ParameterName
                                    + "'),";
                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Parameter ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Parameter (ParameterId"
                                    + ",ParameterName)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
                dataContextDapper.ExecuteSql("SET IDENTITY_INSERT ShopCatalogueSchema.Parameter OFF");
            }

            // --------------------------------------------------------------
            // Product Table INSERT
            // --------------------------------------------------------------
            string productJson = "";

            try
            {
                productJson = System.IO.File.ReadAllText("JSON/Product.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading Product.json file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            IEnumerable<Product>? products = JsonConvert.DeserializeObject<IEnumerable<Product>>(productJson);

            if (products != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Product ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Product (ProductId"
                                    + ",ProductName"
                                    + ",ProductDescription"
                                    + ",ProductImage"
                                    + ",Price"
                                    + ",ManufactureDate"
                                    + ",ExpiryDate"
                                    + ",Rating"
                                    + ",StockQuantity"
                                    + ",SalesCount"
                                    + ",Discount"
                                    + ",SupplierId)"
                                    + "VALUES";
                    foreach (Product singleProduct in products)
                    {
                        string sqlToAdd = "(" + singleProduct.ProductId
                                    + ", '" + singleProduct.ProductName
                                    + "', '" + singleProduct.ProductDescription
                                    + "', '" + singleProduct.ProductImage
                                    + "', " + singleProduct.Price.ToString("0.00", CultureInfo.InvariantCulture)
                                    + ", '" + singleProduct.ManufactureDate.ToString("yyyy-MM-dd")
                                    + "', '" + singleProduct.ExpiryDate?.ToString("yyyy-MM-dd")
                                    + "', '" + singleProduct.Rating.ToString("0.00", CultureInfo.InvariantCulture)
                                    + "', " + singleProduct.StockQuantity
                                    + ", " + singleProduct.SalesCount
                                    + ", " + singleProduct.Discount.ToString("0.00", CultureInfo.InvariantCulture)
                                    + ", " + singleProduct.SupplierId
                                    + "),";
                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"SET IDENTITY_INSERT ShopCatalogueSchema.Product ON;"
                                    + "INSERT INTO ShopCatalogueSchema.Product (ProductId"
                                    + ",ProductName"
                                    + ",ProductDescription"
                                    + ",ProductImage"
                                    + ",Price"
                                    + ",ManufactureDate"
                                    + ",ExpiryDate"
                                    + ",Rating"
                                    + ",StockQuantity"
                                    + ",SalesCount"
                                    + ",Discount"
                                    + ",SupplierId)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
                dataContextDapper.ExecuteSql("SET IDENTITY_INSERT ShopCatalogueSchema.Product OFF");
            }

            // --------------------------------------------------------------
            // ProductCategory Table INSERT
            // --------------------------------------------------------------
            string productCategoryJson = "";

            try
            {
                productCategoryJson = System.IO.File.ReadAllText("JSON/ProductCategory.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading ProductCategory.json file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            IEnumerable<ProductCategory>? productCategories = JsonConvert.DeserializeObject<IEnumerable<ProductCategory>>(productCategoryJson);

            if (productCategories != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"INSERT INTO ShopCatalogueSchema.ProductCategory (CategoryId"
                                    + ",ProductId)"
                                    + "VALUES";
                    foreach (ProductCategory signleProductCategory in productCategories)
                    {
                        string sqlToAdd = "(" + signleProductCategory.CategoryId
                                    + ", " + signleProductCategory.ProductId
                                    + "),";
                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"INSERT INTO ShopCatalogueSchema.ProductCategory (CategoryId"
                                    + ",ProductId)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
            }

            // --------------------------------------------------------------
            // ProductParameter Table INSERT
            // --------------------------------------------------------------
            string productParameterJson = "";

            try
            {
                productParameterJson = System.IO.File.ReadAllText("JSON/ProductParameter.json");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error at reading ProductParameter.json file: " + ex.Message, "Error");
                operationSuccessful = false;
            }

            IEnumerable<ProductParameter>? productParams = JsonConvert.DeserializeObject<IEnumerable<ProductParameter>>(productParameterJson);

            if (productParams != null)
            {
                using (IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
                {
                    string sql = @"INSERT INTO ShopCatalogueSchema.ProductParameter (ParameterId"
                                    + ",ProductId"
                                    + ",ParameterValue)"
                                    + "VALUES";
                    foreach (ProductParameter singleProductParameter in productParams)
                    {
                        string sqlToAdd = "(" + singleProductParameter.ParameterId
                                    + ", " + singleProductParameter.ProductId
                                    + ", '" + singleProductParameter.ParameterValue
                                    + "'),";
                        if ((sql + sqlToAdd).Length > 4000)
                        {
                            dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                            sql = @"INSERT INTO ShopCatalogueSchema.ProductParameter (ParameterId"
                                    + ",ProductId"
                                    + ",ParameterValue)"
                                    + "VALUES";
                        }
                        sql += sqlToAdd;
                    }
                    dataContextDapper.ExecuteProcedureMulti(sql.Trim(','), dbConnection);
                }
            }
            Console.WriteLine("SQL Seed Completed Successfully");

            return operationSuccessful;
        }
    }
}