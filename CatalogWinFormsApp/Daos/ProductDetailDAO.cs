using CatalogWinFormsApp.Data;
using CatalogWinFormsApp.Models;
using Microsoft.Extensions.Configuration;

namespace CatalogWinFormsApp.Daos
{
    internal class ProductDetailDAO
    {
        DataContextDapper _dapper;

        public ProductDetailDAO(IConfiguration config)
        {
            _dapper = new DataContextDapper(config);
        }

        public ProductDetail GetProductDetails(int productId)
        {
			int product = productId;
            string sql = @"
				SELECT 
					Product.ProductId,
					Product.ProductName,
					Product.ProductDescription,
					Product.ProductImage
				FROM ShopCatalogueSchema.Product 
					WHERE Product.ProductId = @product;";

            ProductDetail productDetails = _dapper.LoadDataSingleByProductId<ProductDetail>(sql, product);

            return productDetails;
        }
    }
}