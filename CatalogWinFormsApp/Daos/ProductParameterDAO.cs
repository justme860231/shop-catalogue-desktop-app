using CatalogWinFormsApp.Data;
using CatalogWinFormsApp.Dats;
using Microsoft.Extensions.Configuration;

namespace CatalogWinFormsApp.Daos
{
    internal class ProductParameterDAO
    {

        DataContextDapper _dapper;

        public ProductParameterDAO(IConfiguration config)
        {
            _dapper = new DataContextDapper(config);
        }

        public List<ProductParameterToShow> GetProductParameters(int productId)
        {
            int product = productId;
            string sql = @"
                SELECT
                    Parameter.ParameterId,
	                Parameter.ParameterName,
	                Productparameter.ParameterValue
                FROM ShopCatalogueSchema.Parameter 
                    JOIN ShopCatalogueSchema.ProductParameter ON Parameter.ParameterId = ProductParameter.ParameterId 
	                WHERE ProductParameter.ProductId = @product;";

            List<ProductParameterToShow> productParameters = _dapper.LoadDataByProductId<ProductParameterToShow>(sql, product);

            return productParameters;
        }
    }
}