using CatalogWinFormsApp.Data;
using CatalogWinFormsApp.Models;
using Microsoft.Extensions.Configuration;

namespace CatalogWinFormsApp.Daos
{
    internal class ProductStatusDAO
    {
        DataContextDapper _dapper;

        public ProductStatusDAO(IConfiguration config)
        {
            _dapper = new DataContextDapper(config);
        }

        public List<ProductStatus> GetAllProductsStatus()
        {
            string sql = @"
				SELECT 
					Product.ProductId,
					Product.ProductName,
					(
						SELECT STRING_AGG(Category.CategoryName, ', ')
						FROM ShopCatalogueSchema.ProductCategory 
							JOIN ShopCatalogueSchema.Category ON ProductCategory.CategoryId = Category.CategoryId
							WHERE ProductCategory.ProductId = Product.ProductId
					) AS ProductCategory,
					Product.Price, 
					Product.ManufactureDate, 
					Product.ExpiryDate,
					Product.Rating,
					Product.StockQuantity,
					Product.SalesCount,
					Product.Discount,
					Supplier.SupplierName,
					Supplier.SupplierContact
				FROM ShopCatalogueSchema.Product 
					JOIN ShopCatalogueSchema.Supplier ON Product.SupplierId = Supplier.SupplierId;";

            List<ProductStatus> products = _dapper.LoadData<ProductStatus>(sql);

            return products;
        }

        public List<ProductStatus> SearchProductsStatus(string searchTerm)
        {
			string search = searchTerm;
			string sql = @"
				SELECT 
					Product.ProductId,
					Product.ProductName,
					(
						SELECT STRING_AGG(Category.CategoryName, ', ') AS AllCategories 
						FROM ShopCatalogueSchema.ProductCategory 
							JOIN ShopCatalogueSchema.Category ON ProductCategory.CategoryId = Category.CategoryId
							WHERE ProductCategory.ProductId = Product.ProductId
					) AS ProductCategory,
					Product.Price, 
					Product.ManufactureDate, 
					Product.ExpiryDate,
					Product.Rating,
					Product.StockQuantity,
					Product.SalesCount,
					Product.Discount,
					Supplier.SupplierName,
					Supplier.SupplierContact
				FROM ShopCatalogueSchema.Product 
					JOIN ShopCatalogueSchema.Supplier ON Product.SupplierId = Supplier.SupplierId
					WHERE Product.ProductName LIKE '%' + @search + '%';";

            List<ProductStatus> products = _dapper.LoadDataSearch<ProductStatus>(sql, search);

            return products;
        }
    }
}