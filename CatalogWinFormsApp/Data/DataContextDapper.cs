using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace CatalogWinFormsApp.Data
{
    class DataContextDapper
    {
        private readonly IConfiguration _config;
        public DataContextDapper(IConfiguration config)
        {
            _config = config;
        }

        public List<T> LoadData<T>(string sql)
        {
            List<T> data = new List<T>();

            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.Query<T>(sql).ToList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return data;
        }

        public List<T> LoadDataSearch<T>(string sql, string search)
        {
            List<T> data = new List<T>();

            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.Query<T>(sql, new { search }).ToList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return data;
        }

        public List<T> LoadDataByProductId<T>(string sql, int product)
        {
            List<T> data = new List<T>();

            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.Query<T>(sql, new { product }).ToList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return data;
        }

        public T LoadDataSingle<T>(string sql)
        {
            T data = default(T);
            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.QuerySingle<T>(sql);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return data;
        }

        public T LoadDataSingleByProductId<T>(string sql, int product)
        {
            T data = default(T);
            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.QuerySingle<T>(sql, new { product });
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return data;

        }

        public int ExecuteSql(string sql)
        {
            int data = -1;
            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                data = dbConnection.Execute(sql);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }
            return data;
        }

        public void ExecuteProcedureMulti(string sql, IDbConnection dbConnection)
        {
            try
            {
                dbConnection.Execute(sql);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }
        }

        public bool ExecuteSqlTest(string sql)
        {
            bool result = false;
            try
            {
                IDbConnection dbConnection = new SqlConnection(_config.GetConnectionString("DefaultConnection"));
                result = dbConnection.Execute(sql) > 0;
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Connection is not valid!", "Error");
                Console.WriteLine(ex.Message);
            }

            return result;
        }
    }
}