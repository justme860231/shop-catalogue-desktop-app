namespace CatalogWinFormsApp.Dats
{
    public partial class ProductParameterToShow
    {
        [System.ComponentModel.DisplayName("ID")]
        public int ParameterId { get; set; }
        [System.ComponentModel.DisplayName("Parameter")]
        public string ParameterName { get; set; } = "";
        [System.ComponentModel.DisplayName("Value")]
        public string ParameterValue { get; set; } = "";
    }
}