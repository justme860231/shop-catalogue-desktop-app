﻿namespace CatalogWinFormsApp
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle1 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            productStatsDataGrid = new DataGridView();
            loadDataButton = new Button();
            searchButton = new Button();
            searchTextBox = new TextBox();
            productPictureBox = new PictureBox();
            mainSplitContainer = new SplitContainer();
            productParamsDatagrid = new DataGridView();
            productDetailGroupBox = new GroupBox();
            productDescriptionLabel = new Label();
            productHeaderLabel = new Label();
            welcomeSplitContainer = new SplitContainer();
            connectionStatusPictureBox = new PictureBox();
            connectionToDbLabel = new Label();
            configureDatabaseButton = new Button();
            welcomeHeaderLabel = new Label();
            showCatalogueButton = new Button();
            welcomeTextLabel = new Label();
            ((System.ComponentModel.ISupportInitialize)productStatsDataGrid).BeginInit();
            ((System.ComponentModel.ISupportInitialize)productPictureBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)mainSplitContainer).BeginInit();
            mainSplitContainer.Panel1.SuspendLayout();
            mainSplitContainer.Panel2.SuspendLayout();
            mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)productParamsDatagrid).BeginInit();
            productDetailGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)welcomeSplitContainer).BeginInit();
            welcomeSplitContainer.Panel2.SuspendLayout();
            welcomeSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)connectionStatusPictureBox).BeginInit();
            SuspendLayout();
            // 
            // productStatsDataGrid
            // 
            productStatsDataGrid.AllowUserToAddRows = false;
            productStatsDataGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = SystemColors.Control;
            dataGridViewCellStyle1.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle1.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = DataGridViewTriState.True;
            productStatsDataGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            productStatsDataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = SystemColors.Window;
            dataGridViewCellStyle2.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
            productStatsDataGrid.DefaultCellStyle = dataGridViewCellStyle2;
            productStatsDataGrid.Location = new Point(16, 44);
            productStatsDataGrid.Margin = new Padding(2, 1, 2, 1);
            productStatsDataGrid.MultiSelect = false;
            productStatsDataGrid.Name = "productStatsDataGrid";
            productStatsDataGrid.ReadOnly = true;
            productStatsDataGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            productStatsDataGrid.Size = new Size(964, 192);
            productStatsDataGrid.TabIndex = 5;
            productStatsDataGrid.DataSourceChanged += ProductStatsDataGrid_DataSourceChanged;
            productStatsDataGrid.CellClick += ProductStatsDataGrid_CellClick;
            productStatsDataGrid.KeyDown += productStatsDataGrid_KeyDown;
            // 
            // loadDataButton
            // 
            loadDataButton.Location = new Point(16, 10);
            loadDataButton.Margin = new Padding(2, 1, 2, 1);
            loadDataButton.Name = "loadDataButton";
            loadDataButton.Size = new Size(93, 26);
            loadDataButton.TabIndex = 2;
            loadDataButton.Text = "Reload data";
            loadDataButton.UseVisualStyleBackColor = true;
            loadDataButton.Click += LoadDataButton_Click;
            // 
            // searchButton
            // 
            searchButton.Location = new Point(892, 10);
            searchButton.Name = "searchButton";
            searchButton.Size = new Size(88, 26);
            searchButton.TabIndex = 4;
            searchButton.Text = "Search";
            searchButton.UseVisualStyleBackColor = true;
            searchButton.Click += searchButton_Click;
            // 
            // searchTextBox
            // 
            searchTextBox.Location = new Point(577, 12);
            searchTextBox.Name = "searchTextBox";
            searchTextBox.Size = new Size(294, 22);
            searchTextBox.TabIndex = 3;
            // 
            // productPictureBox
            // 
            productPictureBox.Location = new Point(16, 17);
            productPictureBox.Name = "productPictureBox";
            productPictureBox.Size = new Size(230, 230);
            productPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            productPictureBox.TabIndex = 8;
            productPictureBox.TabStop = false;
            // 
            // mainSplitContainer
            // 
            mainSplitContainer.ImeMode = ImeMode.NoControl;
            mainSplitContainer.Location = new Point(12, 5);
            mainSplitContainer.Name = "mainSplitContainer";
            mainSplitContainer.Orientation = Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            mainSplitContainer.Panel1.Controls.Add(productStatsDataGrid);
            mainSplitContainer.Panel1.Controls.Add(searchButton);
            mainSplitContainer.Panel1.Controls.Add(searchTextBox);
            mainSplitContainer.Panel1.Controls.Add(loadDataButton);
            // 
            // mainSplitContainer.Panel2
            // 
            mainSplitContainer.Panel2.Controls.Add(productParamsDatagrid);
            mainSplitContainer.Panel2.Controls.Add(productDetailGroupBox);
            mainSplitContainer.Panel2.Controls.Add(productPictureBox);
            mainSplitContainer.Size = new Size(995, 513);
            mainSplitContainer.SplitterDistance = 249;
            mainSplitContainer.TabIndex = 9;
            mainSplitContainer.Visible = false;
            // 
            // productParamsDatagrid
            // 
            productParamsDatagrid.AllowUserToAddRows = false;
            productParamsDatagrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = SystemColors.Control;
            dataGridViewCellStyle3.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle3.ForeColor = SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = DataGridViewTriState.True;
            productParamsDatagrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            productParamsDatagrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = SystemColors.Window;
            dataGridViewCellStyle4.Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle4.ForeColor = SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = DataGridViewTriState.False;
            productParamsDatagrid.DefaultCellStyle = dataGridViewCellStyle4;
            productParamsDatagrid.Location = new Point(637, 17);
            productParamsDatagrid.Margin = new Padding(2, 1, 2, 1);
            productParamsDatagrid.MultiSelect = false;
            productParamsDatagrid.Name = "productParamsDatagrid";
            productParamsDatagrid.ReadOnly = true;
            productParamsDatagrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            productParamsDatagrid.Size = new Size(343, 230);
            productParamsDatagrid.TabIndex = 6;
            // 
            // productDetailGroupBox
            // 
            productDetailGroupBox.Controls.Add(productDescriptionLabel);
            productDetailGroupBox.Controls.Add(productHeaderLabel);
            productDetailGroupBox.Location = new Point(276, 9);
            productDetailGroupBox.Name = "productDetailGroupBox";
            productDetailGroupBox.Size = new Size(334, 238);
            productDetailGroupBox.TabIndex = 10;
            productDetailGroupBox.TabStop = false;
            productDetailGroupBox.Text = "Product Detail";
            // 
            // productDescriptionLabel
            // 
            productDescriptionLabel.AutoSize = true;
            productDescriptionLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            productDescriptionLabel.Location = new Point(15, 78);
            productDescriptionLabel.MaximumSize = new Size(300, 150);
            productDescriptionLabel.Name = "productDescriptionLabel";
            productDescriptionLabel.Size = new Size(79, 17);
            productDescriptionLabel.TabIndex = 10;
            productDescriptionLabel.Text = "Product info";
            // 
            // productHeaderLabel
            // 
            productHeaderLabel.AutoSize = true;
            productHeaderLabel.Font = new Font("Segoe UI", 15.75F, FontStyle.Bold, GraphicsUnit.Point);
            productHeaderLabel.Location = new Point(6, 18);
            productHeaderLabel.MaximumSize = new Size(340, 60);
            productHeaderLabel.Name = "productHeaderLabel";
            productHeaderLabel.Size = new Size(91, 30);
            productHeaderLabel.TabIndex = 9;
            productHeaderLabel.Text = "Product";
            // 
            // welcomeSplitContainer
            // 
            welcomeSplitContainer.Location = new Point(6, 102);
            welcomeSplitContainer.Name = "welcomeSplitContainer";
            welcomeSplitContainer.Orientation = Orientation.Horizontal;
            welcomeSplitContainer.Panel1Collapsed = true;
            // 
            // welcomeSplitContainer.Panel2
            // 
            welcomeSplitContainer.Panel2.Controls.Add(connectionStatusPictureBox);
            welcomeSplitContainer.Panel2.Controls.Add(connectionToDbLabel);
            welcomeSplitContainer.Panel2.Controls.Add(configureDatabaseButton);
            welcomeSplitContainer.Panel2.Controls.Add(welcomeHeaderLabel);
            welcomeSplitContainer.Panel2.Controls.Add(showCatalogueButton);
            welcomeSplitContainer.Panel2.Controls.Add(welcomeTextLabel);
            welcomeSplitContainer.Size = new Size(1004, 416);
            welcomeSplitContainer.SplitterDistance = 25;
            welcomeSplitContainer.TabIndex = 10;
            // 
            // connectionStatusPictureBox
            // 
            connectionStatusPictureBox.Location = new Point(167, 388);
            connectionStatusPictureBox.MaximumSize = new Size(15, 15);
            connectionStatusPictureBox.Name = "connectionStatusPictureBox";
            connectionStatusPictureBox.Size = new Size(15, 15);
            connectionStatusPictureBox.TabIndex = 4;
            connectionStatusPictureBox.TabStop = false;
            // 
            // connectionToDbLabel
            // 
            connectionToDbLabel.AutoSize = true;
            connectionToDbLabel.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            connectionToDbLabel.Location = new Point(22, 388);
            connectionToDbLabel.MaximumSize = new Size(500, 300);
            connectionToDbLabel.Name = "connectionToDbLabel";
            connectionToDbLabel.Size = new Size(139, 15);
            connectionToDbLabel.TabIndex = 3;
            connectionToDbLabel.Text = "Connection to DB Status:";
            connectionToDbLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // configureDatabaseButton
            // 
            configureDatabaseButton.Location = new Point(308, 245);
            configureDatabaseButton.Name = "configureDatabaseButton";
            configureDatabaseButton.Size = new Size(121, 23);
            configureDatabaseButton.TabIndex = 2;
            configureDatabaseButton.Text = "Configure DB";
            configureDatabaseButton.UseVisualStyleBackColor = true;
            configureDatabaseButton.Click += ConfigureDatabaseButton_Click;
            // 
            // welcomeHeaderLabel
            // 
            welcomeHeaderLabel.AutoSize = true;
            welcomeHeaderLabel.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            welcomeHeaderLabel.Location = new Point(253, 66);
            welcomeHeaderLabel.Name = "welcomeHeaderLabel";
            welcomeHeaderLabel.Size = new Size(496, 32);
            welcomeHeaderLabel.TabIndex = 0;
            welcomeHeaderLabel.Text = "Welcome to the Shop Catalogue Manager!";
            // 
            // showCatalogueButton
            // 
            showCatalogueButton.Location = new Point(583, 245);
            showCatalogueButton.Name = "showCatalogueButton";
            showCatalogueButton.Size = new Size(121, 23);
            showCatalogueButton.TabIndex = 1;
            showCatalogueButton.Text = "Catalogue";
            showCatalogueButton.UseVisualStyleBackColor = true;
            showCatalogueButton.Click += ShowCatalogueButton_Click;
            // 
            // welcomeTextLabel
            // 
            welcomeTextLabel.AutoSize = true;
            welcomeTextLabel.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point);
            welcomeTextLabel.Location = new Point(258, 145);
            welcomeTextLabel.MaximumSize = new Size(500, 300);
            welcomeTextLabel.Name = "welcomeTextLabel";
            welcomeTextLabel.Size = new Size(484, 60);
            welcomeTextLabel.TabIndex = 0;
            welcomeTextLabel.Text = resources.GetString("welcomeTextLabel.Text");
            welcomeTextLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1022, 523);
            Controls.Add(welcomeSplitContainer);
            Controls.Add(mainSplitContainer);
            Font = new Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point);
            Margin = new Padding(2, 3, 2, 3);
            MinimumSize = new Size(1038, 562);
            Name = "MainForm";
            Text = "Shop Catalogue";
            Shown += MainForm_Shown;
            ((System.ComponentModel.ISupportInitialize)productStatsDataGrid).EndInit();
            ((System.ComponentModel.ISupportInitialize)productPictureBox).EndInit();
            mainSplitContainer.Panel1.ResumeLayout(false);
            mainSplitContainer.Panel1.PerformLayout();
            mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)mainSplitContainer).EndInit();
            mainSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)productParamsDatagrid).EndInit();
            productDetailGroupBox.ResumeLayout(false);
            productDetailGroupBox.PerformLayout();
            welcomeSplitContainer.Panel2.ResumeLayout(false);
            welcomeSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)welcomeSplitContainer).EndInit();
            welcomeSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)connectionStatusPictureBox).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private DataGridView productStatsDataGrid;
        private Button loadDataButton;
        private Button searchButton;
        private TextBox searchTextBox;
        private PictureBox productPictureBox;
        private SplitContainer mainSplitContainer;
        private Label productHeaderLabel;
        private GroupBox productDetailGroupBox;
        private DataGridView productParamsDatagrid;
        private Label productDescriptionLabel;
        private SplitContainer welcomeSplitContainer;
        private Label welcomeHeaderLabel;
        private Label welcomeTextLabel;
        private Button showCatalogueButton;
        private Button configureDatabaseButton;
        private PictureBox connectionStatusPictureBox;
        private Label connectionToDbLabel;
    }
}