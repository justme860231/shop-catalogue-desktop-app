using CatalogWinFormsApp.Daos;
using CatalogWinFormsApp.Models;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;

namespace CatalogWinFormsApp
{
    public partial class MainForm : Form
    {
        BindingSource productBindingSource = new BindingSource();
        BindingSource paramsBindingSource = new BindingSource();
        IConfiguration config;

        public MainForm()
        {
            InitializeComponent();
            InitializeLayout();
        }

        private void InitializeLayout()
        {
            mainSplitContainer.Visible = false;
            welcomeSplitContainer.Visible = true;
            showCatalogueButton.Enabled = false;
            connectionStatusPictureBox.Image = Image.FromFile(@"Images\red-circle.png");
        }

        private void TestConnection()
        {
            Console.WriteLine("Test Connection");

            config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            IDbConnection dbConnection = new SqlConnection(config.GetConnectionString("DefaultConnection"));

            try
            {
                DateTime outputTime = dbConnection.QuerySingle<DateTime>("SELECT GETDATE();");
                showCatalogueButton.Enabled = true;
                connectionStatusPictureBox.Image = Image.FromFile(@"Images\green-circle.png");
            }
            catch (Exception ex)
            {
                showCatalogueButton.Enabled = false;
                connectionStatusPictureBox.Image = Image.FromFile(@"Images\red-circle.png");
                MessageBox.Show("Error: " + ex.Message, "Error");
            }
        }

        private void SelectProduct(int rowClicked)
        {

            if (productStatsDataGrid.Rows[rowClicked].Cells[1].Value != DBNull.Value)
            {
                productStatsDataGrid.Rows[rowClicked].Selected = true;

                int productId = Convert.ToInt32(productStatsDataGrid.Rows[rowClicked].Cells[0].Value);

                ProductDetailDAO productDetailDAO = new ProductDetailDAO(config);

                ProductDetail productDetails = productDetailDAO.GetProductDetails(productId);

                productPictureBox.Image = Image.FromFile(@"Images\" + productDetails.ProductImage);
                productHeaderLabel.Text = productDetails.ProductName;
                productDescriptionLabel.Text = productDetails.ProductDescription;

                ProductParameterDAO productParametersDAO = new ProductParameterDAO(config);

                paramsBindingSource.DataSource = productParametersDAO.GetProductParameters(productId);

                productParamsDatagrid.DataSource = paramsBindingSource;
            }
        }

        private void ShowCatalogueButton_Click(object sender, EventArgs e)
        {
            mainSplitContainer.Visible = true;
            welcomeSplitContainer.Visible = false;

            ProductStatusDAO productStatusesDAO = new ProductStatusDAO(config);

            productBindingSource.DataSource = productStatusesDAO.GetAllProductsStatus();

            productStatsDataGrid.DataSource = productBindingSource;
        }

        private void LoadDataButton_Click(object sender, EventArgs e)
        {
            ProductStatusDAO productStatusesDAO = new ProductStatusDAO(config);

            productBindingSource.DataSource = productStatusesDAO.GetAllProductsStatus();

            productStatsDataGrid.DataSource = productBindingSource;

            SelectProduct(0);
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            ProductStatusDAO productStatusesDAO = new ProductStatusDAO(config);

            productBindingSource.DataSource = productStatusesDAO.SearchProductsStatus(searchTextBox.Text);

            productStatsDataGrid.DataSource = productBindingSource;

            SelectProduct(0);

        }

        private void ProductStatsDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;

            int rowClicked = dataGridView.CurrentRow.Index;

            SelectProduct(rowClicked);
        }

        private void ProductStatsDataGrid_DataSourceChanged(object sender, EventArgs e)
        {
            SelectProduct(0);
        }

        private void ConfigureDatabaseButton_Click(object sender, EventArgs e)
        {
            Form configForm = new ConfigForm(this.TestConnection);
            configForm.Show();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            TestConnection();
        }

        private void productStatsDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;

            if (dataGridView.CurrentRow.Index < dataGridView.Rows.Count - 1 && e.KeyValue == 40)
            {
                SelectProduct(dataGridView.CurrentRow.Index + 1);
            }
            else if (dataGridView.CurrentRow.Index > 0 && e.KeyValue == 38)
            {
                SelectProduct(dataGridView.CurrentRow.Index - 1);
            }
        }
    }
}