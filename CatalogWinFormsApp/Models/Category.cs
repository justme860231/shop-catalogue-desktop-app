namespace CatalogWinFormsApp.Models
{
    public partial class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; } = "";
        public string SupplierContact { get; set; } = "";  
    }
}