namespace CatalogWinFormsApp.Models
{
    public partial class Parameter
    {
        public int ParameterId { get; set; }
        public string ParameterName { get; set; } = "";
        public string ParameterValue { get; set; } = "";  
    }
}