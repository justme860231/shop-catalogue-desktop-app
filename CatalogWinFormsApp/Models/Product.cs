namespace CatalogWinFormsApp.Models
{
    public partial class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; } = "";
        public string ProductDescription { get; set; } = "";
        public string ProductImage { get; set; } = "";
        public decimal Price { get; set; }
        public DateTime ManufactureDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public decimal Rating { get; set; }
        public int StockQuantity { get; set; }
        public int SalesCount { get; set; }
        public decimal Discount { get; set; }
        public int SupplierId { get; set; }
    }
}