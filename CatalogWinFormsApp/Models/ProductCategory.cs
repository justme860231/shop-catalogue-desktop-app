namespace CatalogWinFormsApp.Models
{
    public partial class ProductCategory
    {
        public int CategoryId { get; set; }
        public int ProductId { get; set; }
    }
}