namespace CatalogWinFormsApp.Models
{
    public partial class ProductParameter
    {
        public int ParameterId { get; set; }
        public int ProductId { get; set; }
        public string ParameterValue { get; set; } = "";
    }
}