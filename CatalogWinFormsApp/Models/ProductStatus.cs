namespace CatalogWinFormsApp.Models
{
    public partial class ProductStatus
    {
        [System.ComponentModel.DisplayName("ID")]
        public int ProductId { get; set; }
        [System.ComponentModel.DisplayName("Product")]
        public string ProductName { get; set; } = "";
        [System.ComponentModel.DisplayName("Categories")]
        public string ProductCategory { get; set; } = "";
        [System.ComponentModel.DisplayName("Price (�)")]
        public decimal Price { get; set; }
        [System.ComponentModel.DisplayName("Manufacture Date")]
        public DateTime? ManufactureDate { get; set; }
        [System.ComponentModel.DisplayName("Expiration Date")]
        public DateTime ExpiryDate { get; set; }
        [System.ComponentModel.DisplayName("Rating")]
        public decimal Rating { get; set; }
        [System.ComponentModel.DisplayName("Stock Level")]
        public int StockQuantity { get; set; }
        [System.ComponentModel.DisplayName("Sales Total")]
        public int SalesCount { get; set; }
        [System.ComponentModel.DisplayName("Discount")]
        public decimal Discount { get; set; }
        [System.ComponentModel.DisplayName("Supplier")]
        public string SupplierName { get; set; } = "";
        [System.ComponentModel.DisplayName("Supplier Contact")]
        public string SupplierContact { get; set; } = "";
    }
}