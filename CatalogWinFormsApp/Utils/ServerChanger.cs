namespace CatalogWinFormsApp.Utils
{
    public class ServerChanger
    {
        private readonly string _filePath;

        public ServerChanger(string filePath)
        {
            _filePath = filePath;
        }

        public void SetServerName(string serverName)
        {
            string appsettingsJson = System.IO.File.ReadAllText(_filePath);

            dynamic? jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(appsettingsJson);

            List<string> connectionStrings = new List<string> { "DefaultConnection", "StartingConnection" };
            if (jsonObj != null)
            {
                for (int i = 0; i < 2; i++)
                {
                    string connectionString = jsonObj["ConnectionStrings"][connectionStrings[i]];

                    List<string> connectionStringList = connectionString.Split(";").ToList();
                    List<string> serverDefinitionList = connectionStringList[0].Split("=").ToList();
                
                    serverDefinitionList[1] = CheckSpecialChar(serverName);

                    connectionStringList[0] = String.Join("=", serverDefinitionList.ToArray());
                
                    jsonObj["ConnectionStrings"][connectionStrings[i]] = String.Join(";", connectionStringList.ToArray());

                    File.WriteAllText("appsettings.json", "" + jsonObj);

                }
            }
        }

        public static string CheckSpecialChar(string name)
        {
            List<string> nameList = name.Split("").ToList();
            List<string> result = new();
            
            for (int i = 0; i < nameList.Count ; i++)
            {
                if (nameList[i] == "\\")
                {
                    result.Add(nameList[i]);
                }
                result.Add(nameList[i]);
                i = i + 1;
            }

            return String.Join("", result.ToArray());
        }
    }
}