-- -- ----------------------------------------------------------
-- -- CREATE Tables-- DROP Tables if exists
-- -- ----------------------------------------------------------
DROP TABLE IF EXISTS ShopCatalogueSchema.ProductParameter;
CREATE TABLE ShopCatalogueSchema.ProductParameter
(
	ParameterId INT NOT NULL,
	ProductId INT NOT NULL,
	ParameterValue NVARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS ShopCatalogueSchema.ProductCategory;
CREATE TABLE ShopCatalogueSchema.ProductCategory
(
	ProductId INT NOT NULL,
	CategoryId INT NOT NULL,
);

DROP TABLE IF EXISTS ShopCatalogueSchema.Product;
CREATE TABLE ShopCatalogueSchema.Product
(
	ProductId INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
	ProductName NVARCHAR(50) NOT NULL,
	ProductDescription NVARCHAR(255) NOT NULL,
	ProductImage NVARCHAR(255),
	Price DECIMAL(18, 2) NOT NULL,
	ManufactureDate DATETIME NOT NULL,
	ExpiryDate DATETIME,
	Rating DECIMAL(2,1),
	StockQuantity INT NOT NULL,
	SalesCount INT,
	Discount DECIMAL(3, 2),
	SupplierId INT NOT NULL,
);

DROP TABLE IF EXISTS ShopCatalogueSchema.Supplier;
CREATE TABLE ShopCatalogueSchema.Supplier
(
	SupplierId INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
	SupplierName NVARCHAR(50) NOT NULL,
	SupplierContact NVARCHAR(255) NOT NULL
);

DROP TABLE IF EXISTS ShopCatalogueSchema.Category;
CREATE TABLE ShopCatalogueSchema.Category
(
	CategoryId INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
	CategoryName NVARCHAR(50) NOT NULL,
);

DROP TABLE IF EXISTS ShopCatalogueSchema.Parameter;
CREATE TABLE ShopCatalogueSchema.Parameter
(
	ParameterId INT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
	ParameterName NVARCHAR(50) NOT NULL,
);


ALTER TABLE ShopCatalogueSchema.Product  
ADD CONSTRAINT FK_Product_SupplierId FOREIGN KEY (SupplierId)     
    REFERENCES ShopCatalogueSchema.Supplier (SupplierId)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE;

ALTER TABLE ShopCatalogueSchema.ProductCategory  
ADD CONSTRAINT FK_ProductCategory_ProductId FOREIGN KEY (ProductId)     
    REFERENCES ShopCatalogueSchema.Product (ProductId)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE;

ALTER TABLE ShopCatalogueSchema.ProductCategory  
ADD CONSTRAINT FK_ProductCategory_CategoryId FOREIGN KEY (CategoryId)     
    REFERENCES ShopCatalogueSchema.Category (CategoryId)     
    ON DELETE NO ACTION    
   ON UPDATE NO ACTION;

ALTER TABLE ShopCatalogueSchema.ProductParameter  
ADD CONSTRAINT FK_ProductParameter_ParameterId FOREIGN KEY (ParameterId)     
    REFERENCES ShopCatalogueSchema.Parameter (ParameterId)     
    ON DELETE CASCADE    
    ON UPDATE CASCADE;

ALTER TABLE ShopCatalogueSchema.ProductParameter  
ADD CONSTRAINT FK_ProductParameter_ProductId FOREIGN KEY (ProductId)     
    REFERENCES ShopCatalogueSchema.Product (ProductId)     
    ON DELETE NO ACTION    
    ON UPDATE NO ACTION;