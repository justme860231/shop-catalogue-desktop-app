DECLARE @schemaExists INT;

-- Check if the schema exists
SELECT @schemaExists = COUNT(*)
FROM INFORMATION_SCHEMA.SCHEMATA
WHERE SCHEMA_NAME = 'ShopCatalogueSchema';

-- If the schema exists, drop it
IF @schemaExists > 0
BEGIN
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.ProductParameter')
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.ProductCategory')
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.Product')
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.Supplier')
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.Category')
    EXEC('DROP TABLE IF EXISTS ShopCatalogueSchema.Parameter')
    EXEC('DROP SCHEMA ShopCatalogueSchema');
END

-- Create a new schema
IF SCHEMA_ID('ShopCatalogueSchema') IS NULL
BEGIN
    EXEC('CREATE SCHEMA ShopCatalogueSchema');
END