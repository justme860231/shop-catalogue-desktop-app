# Shop Catalogue - Desktop app



## Description

This desktop application built with DevExpress serves as a Shop Catalogue tailored for shop owners. After instalation of this desktop application users are prompted to establish their dedicated database. The application provides an initial configuration interface on the starting page for setting DB, Schema and Tables with data. Furthermore, users have the flexibility to modify the preconfigured application's Server name directly within the Desktop application.

Once a successful connection is established between the application and the database, users can seamlessly delve into the Catalogue functionality. The Catalogue is divided into two key segments. The first segment encompasses essential product information, while the second segment offers in-depth insights into the selected product, providing a more comprehensive view.

## DB Schema

The DB Schema comprises a total of 6 Tables, with two of them serving as junction tables to facilitate many-to-many relationships. The primary Table is denoted as <b>Product</b>, housing comprehensive details pertaining to all essential parameters. Following this, the <b>Supplier</b> Table contains the name and contact information of specific product suppliers.

Each product is categorized under one or more categories, necessitating the inclusion of the <b>Category</b> Table. To accommodate the potential for multiple categories per product, the <b>ProductCategory</b> Table serves as a junction point, establishing many-to-many relationships. This table solely forges connections between categories and products, effectively creating pairs between the two.

Lastly, the schema incorporates two additional tables dedicated to parameter assignment for products: <b>Parameter</b> and <b>ProductParameter</b>. The synergy between these two tables and the <b>Product</b> Table establishes links between various parameters specific to each product.

<div align="center">
<img src="docs/DB-diagram.png" alt="Diagram of the table relations inside database.">
</div>

## Software and Tools

I was working with this software and tools:
- DevExpress
- SQL 2022
- SQL Server Management Studio (SSMS)
- Visual Studio 2022
- .NET SDK x64

## Installation

### SQL Server

Before runing project or application please configure your SQL server. You can do it easily with <b>Server Management Studio</b> (SSMS). You need to configure Server Name and set Authentication. If you are Windows user you just need to select <b>Windows Authentication</b> and set the name of the server. There are three options:
1. Set the Server Name as it is down below (default application settings of desktop application):
```
Server Name: (localdb)\MSSQLLocalDB
```
2. Set your own name and change it at the startup of the application.
3. Manually, change the value inside `appsettings.json` file (connection string value).

### Application

Select and run instalation using instalation file from directory `CatalogWinFormsSetup/Release/setup.exe`. After instalation application you can run and set the application.

### Project 

Run the application with Microsoft <b>Visual Studio</b>. Simple open `CatalogWinFormsApp.sln` file with Visual Studio and project is ready to Build and Run.


## Preview

Application starts with welcome screen with small introduction. On the bottom there is the status of the connection to SQL Server. User has two options, configure Database or go right in to the Catalogue section.

<div align="center">
<img src="docs/WelcomePag.png" width="80%" height="80%" align="center" alt="Printscreen of welcome page of the desktop applicatino">
</div>

Clicking on configuration button user opens new Form with option to configure the Database. User can change Server Name for `ConnectionString`. It has to be same name that you defined at setting SQL Server. After that there are three checkboxes for:
- creating the Database
- creating the Schema
- filling the Tables with preapared data

<div align="center" >
<img src="docs/ConfigPage.png" width="50%" height="50%" align="center" alt="Printscreen of the form with configuration of database.">
</div>

After successful configuration, users returns to the main Welcome Form, where he can continues to the main Catalogue section. Catalogue can be from layout perspective split to two parts. Upper section has `dataGridView` with products informations. User can uses search bar for quicker finding of the products. Depends on which product he chooses, this product informations are shown in bottom section, with more details about product. 

<div align="center" >
<img src="docs/CataloguePage.png" width="80%" height="80%" align="center" alt="Printscreen of the form with configuration of database.">
</div>
